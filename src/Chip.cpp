#include "Chip.h"

namespace Arkanoid {

Chip::Chip(float x, float y, float width, float height, Color color, BonusType bonus)
		: mBonus(bonus) {
	mVertices[0] = Vertex(x, y, color);
    mVertices[1] = Vertex(x, y + height, color);
    mVertices[2] = Vertex(x + width, y + height, color);
    mVertices[3] = Vertex(x + width, y, color);
}

float Chip::Left() const {
	return mVertices[0].x;
}

float Chip::Right() const {
	return mVertices[2].x;
}

float Chip::Top() const {
	return mVertices[1].y;
}

float Chip::Bottom() const {
	return mVertices[0].y;
}

BonusType Chip::Bonus() const {
	return mBonus;
}

void Chip::Draw() {
	Gfx.DrawTriangleFan(&mVertices[0], 4);
	Gfx.BlendMode().Push(BLEND_ADD);
    Gfx.SetLineWidth(2.5);
    Gfx.DrawLineLoop(&mVertices[0], 4);
    Gfx.SetLineWidth(1.75);
    Gfx.DrawLineLoop(&mVertices[0], 4);
    Gfx.SetLineWidth(1);
    Gfx.DrawLineLoop(&mVertices[0], 4);
    Gfx.BlendMode().Pop();
}

} /* namespace Arkanoid */
