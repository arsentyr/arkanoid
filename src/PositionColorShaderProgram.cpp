#include "PositionColorShaderProgram.h"

#include "Vertex.h"

namespace Arkanoid {
namespace {

const GLchar srcPositionColorVertexShader[] =
    "attribute vec2 a_position;\n"
    "attribute vec4 a_color;\n"
    "uniform mat4 u_model_view_matrix;\n"
    "uniform mat4 u_projection_matrix;\n"
    "uniform vec4 u_color;\n"
    "varying mediump vec4 v_color;\n"
    "mat4 mvp_matrix = u_projection_matrix * u_model_view_matrix;\n"
    "void main() {\n"
    "  v_color = a_color * u_color;\n"
    "  gl_Position = mvp_matrix * vec4(a_position.x, a_position.y, 0, 1);\n"
    "}\n";

const GLchar srcPositionColorFragmentShader[] =
    "precision lowp float;\n"
    "varying vec4 v_color;\n"
    "void main() {\n"
    "  gl_FragColor = v_color;\n"
    "}\n";

} /* anonymous namespace */

PositionColorShaderProgram::PositionColorShaderProgram()
        : mUniformModelViewMatrixLocation(UNKNOWN_UNIFORM_LOCATION)
        , mUniformProjectionMatrixLocation(UNKNOWN_UNIFORM_LOCATION)
        , mUniformColorLocation(UNKNOWN_UNIFORM_LOCATION) {
}

void PositionColorShaderProgram::Load() {
    ASSERT(mUniformModelViewMatrixLocation == UNKNOWN_UNIFORM_LOCATION);
    Create(srcPositionColorVertexShader, srcPositionColorFragmentShader);
    BindAttributeLocation(ATTRIBUTE_POSITION_INDEX, "a_position");
    BindAttributeLocation(ATTRIBUTE_COLOR_INDEX, "a_color");
    Link();
    mUniformModelViewMatrixLocation = GetUniformLocation("u_model_view_matrix");
    mUniformProjectionMatrixLocation = GetUniformLocation("u_projection_matrix");
    mUniformColorLocation = GetUniformLocation("u_color");
	Gfx.BlendMode().Push(BLEND_ALPHA);
}

void PositionColorShaderProgram::Unload() {
    Gfx.BlendMode().Pop();
    Delete();
    mUniformModelViewMatrixLocation = UNKNOWN_UNIFORM_LOCATION;
    mUniformProjectionMatrixLocation = UNKNOWN_UNIFORM_LOCATION;
    mUniformColorLocation = UNKNOWN_UNIFORM_LOCATION;   
}

void PositionColorShaderProgram::Reload() {
    Lose();
    Load();
}

void PositionColorShaderProgram::MvpMatrixChanged() {
    SetUniformMatrix4fv(mUniformModelViewMatrixLocation, 1, Gfx.ModelView().Top());
    SetUniformMatrix4fv(mUniformProjectionMatrixLocation, 1, Gfx.Projection().Top());
}

void PositionColorShaderProgram::ColorChanged() {
    SetUniform4fv(mUniformColorLocation, 1, Gfx.Color().Top());
}

void PositionColorShaderProgram::SetVertexAttributes(const void* vertices) {
    const GLsizei stride = sizeof(Vertex);
    const GLfloat* pointer = static_cast<const GLfloat*>(vertices);
    SetVertexAttributePointer(ATTRIBUTE_POSITION_INDEX, 2, GL_FLOAT, GL_FALSE, stride, pointer);
    SetVertexAttributePointer(ATTRIBUTE_COLOR_INDEX, 4, GL_FLOAT, GL_FALSE, stride, (pointer + 2));
    EnableVertexAttributeArray(ATTRIBUTE_POSITION_INDEX);
    EnableVertexAttributeArray(ATTRIBUTE_COLOR_INDEX);
}

} /* namespace Arkanoid */
