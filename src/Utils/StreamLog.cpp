#include "Utils/StreamLog.h"

#include <cstdarg>
#include <cstdio>
#include "Assert.h"
#include "Config.h"

namespace Core {

void StreamLog::Verbose(const char* format, ...) {
    ASSERT(format != NULL);
    if (Config::LOGGABLE_VERBOSE) {
        va_list args;
        va_start(args, format);
        fprintf(stderr, "Verbose: ");
        fprintf(stderr, format, args);
        va_end(args);
        fprintf(stderr, "\n");
    }
}

void StreamLog::Debug(const char* format, ...) {
    ASSERT(format != NULL);
    if (Config::LOGGABLE_DEBUG) {
        va_list args;
        va_start(args, format);
        fprintf(stderr, "Debug: ");
        fprintf(stderr, format, args);
        va_end(args);
        fprintf(stderr, "\n");
    }
}

void StreamLog::Warning(const char* format, ...) {
    ASSERT(format != NULL);
    if (Config::LOGGABLE_WARNING) {
        va_list args;
        va_start(args, format);
        fprintf(stderr, "Warning: ");
        fprintf(stderr, format, args);
        va_end(args);
        fprintf(stderr, "\n");
    }
}

void StreamLog::Error(const char* format, ...) {
    ASSERT(format != NULL);
    if (Config::LOGGABLE_ERROR) {
        va_list args;
        va_start(args, format);
        fprintf(stderr, "Error: ");
        fprintf(stderr, format, args);
        va_end(args);
        fprintf(stderr, "\n");
    }
}

namespace {

StreamLog streamLog;

} /* anonymous namespace */

ILog& Log = streamLog;

} /* namespace Core */
