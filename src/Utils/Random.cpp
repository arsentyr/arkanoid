#include "Utils/Random.h"

#include <cstdlib>
#include <ctime>
#include "Assert.h"

namespace Core {

Random::Random() {
	srand(static_cast<unsigned>(time(0)));
}

bool Random::NextBool() {
    return NextInt(0, 10) < 5;
}

int Random::NextInt(int min, int max) {
    ASSERT(min <= max);
    return rand() % (max - min + 1) + min;
}

} /* namespace Core */
