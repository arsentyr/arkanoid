#include "Utils/Timer.h"

#include "Assert.h"

namespace Core {

Timer::Timer()
        : mIsStarted(false) {
}

void Timer::Start() {
    ASSERT(!mIsStarted);
    mTime = clock();
    mIsStarted = true;
}

void Timer::Stop() {
    ASSERT(mIsStarted);
    mIsStarted = false;
}

float Timer::GetDeltaTime() {
    ASSERT(mIsStarted);
    if (!mIsStarted) {
        return 0;
    }

    const clock_t now = clock();
    const float deltaTime = static_cast<float>((now - mTime)) / CLOCKS_PER_SEC;
    mTime = now;
    return deltaTime;
}

} /* namespace Core */
