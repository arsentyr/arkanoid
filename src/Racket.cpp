#include "Racket.h"

namespace Arkanoid {

void Racket::SetLocation(float x, float y, float width, float height) {
    const Color color(32 / 255.f, 170 / 255.f, 214 / 255.f, 92 / 255.f);
    mVertices[0] = Vertex(x, y, color);
    mVertices[1] = Vertex(x, y + height, color);
    mVertices[2] = Vertex(x + width, y + height, color);
    mVertices[3] = Vertex(x + width, y, color);
}

void Racket::SetOffsetX(float offsetX) {
    mVertices[0].x += offsetX;
    mVertices[1].x += offsetX;
    mVertices[2].x += offsetX;
    mVertices[3].x += offsetX;
}

float Racket::Left() const {
    return mVertices[0].x;
}

float Racket::Right() const {
    return mVertices[2].x;
}

float Racket::Top() const {
    return mVertices[1].y;
}

float Racket::Bottom() const {
    return mVertices[0].y;
}

bool Racket::Hit(const Ball& ball) const {
    float distance = 0;
    const Vertex& leftBottom = mVertices[0];
    const Vertex& rightTop = mVertices[2];
    if (ball.x < leftBottom.x) {
        const float e = ball.x - leftBottom.x;
        distance += e * e;
    }
    else if (ball.x > rightTop.x) {
        const float e = ball.x - rightTop.x;
        distance += e * e;
    }
    if (ball.y < leftBottom.y) {
        const float e = ball.y - leftBottom.y;
        distance += e * e;
    }
    else if (ball.y > rightTop.y) {
        const float e = ball.y - rightTop.y;
        distance += e * e;
    }
    return distance <= ball.Radius() * ball.Radius();
}

bool Racket::Hit(const Bonus& bonus) const {
    if (Left() < bonus.Left()) {
        if (Right() < bonus.Left()) {
            return false;
        }
    }
    else if (Left() > bonus.Right()) {
        return false;
    }
    if (Bottom() < bonus.Bottom()) {
        if (Top() < bonus.Bottom()) {
            return false;
        }
    }
    else if (Bottom() > bonus.Top()) {
        return false;
    }
    return true;
}

void Racket::Draw() {
    Gfx.DrawTriangleFan(&mVertices[0], 4);
    Gfx.BlendMode().Push(BLEND_ADD);
    Gfx.SetLineWidth(3);
    Gfx.DrawLineLoop(&mVertices[0], 4);
    Gfx.SetLineWidth(2);
    Gfx.DrawLineLoop(&mVertices[0], 4);
    Gfx.SetLineWidth(1);
    Gfx.DrawLineLoop(&mVertices[0], 4);
    Gfx.BlendMode().Pop();
}

} /* namespace Arkanoid */
