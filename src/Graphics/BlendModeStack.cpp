#include "Graphics/BlendModeStack.h"

#include "Assert.h"
#include "Graphics/GLES2.h"

namespace Core { 

BlendModeStack::BlendModeStack(unsigned int capacity) {
    ASSERT(capacity > 0);
    mStack.reserve(capacity);
    mStack.push_back(BLEND_NORMAL);
}

BlendModeStack& BlendModeStack::operator=(BlendMode blendMode) {
    SetBlendMode(blendMode);
    mStack.back() = blendMode;
    return *this;
}

BlendModeStack::operator BlendMode () const {
    return mStack.back();
}

void BlendModeStack::Push(BlendMode blendMode) {
    ASSERT(mStack.size() < mStack.capacity());
    SetBlendMode(blendMode);
    mStack.push_back(blendMode);
}

void BlendModeStack::Pop() {
    VERIFY(mStack.size() > 1, "Blend mode stack underflow.");
    mStack.pop_back();
    SetBlendMode(mStack.back());
}

BlendMode BlendModeStack::Top() const {
    return mStack.back();
}

unsigned int BlendModeStack::Depth() const {
    return mStack.size();
}

void BlendModeStack::SetBlendMode(BlendMode blendMode) {        
    glBlendEquation(GL_FUNC_ADD);
    switch (blendMode) {
    case BLEND_NORMAL:
        glBlendFunc(GL_ONE, GL_ONE);
        break;
    case BLEND_ALPHA:
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        break;
    case BLEND_ADD:
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);
        break;
    case BLEND_MULTIPLY:
        glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
        break;
    }
    CHECK_GL();
}

} /* namespace Core */
