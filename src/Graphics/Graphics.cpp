#include "Graphics/Graphics.h"

#include "Assert.h"

namespace Core {

Graphics::Graphics()
        : mState(STATE_UNITIALIZED)
        , mModelViewMatrixStack(8)
        , mProjectionMatrixStack(2)
		, mProjectionMatrix(Matrix4::Identity())
        , mColorStack(8)
        , mBlendModeStack(8)
        , mShaderProgram(NULL) {
}

Graphics::~Graphics() {
    ASSERT(mState == STATE_RELEASED);
    if (mState != STATE_RELEASED) {
        Release();
    } 
}

void Graphics::Init() {
    ASSERT(mState == STATE_UNITIALIZED);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    CHECK_GL();
    mState = STATE_INITIALIZED;
}

void Graphics::Reload() {
    ASSERT(mState == STATE_INITIALIZED || mState == STATE_FRAMED);
    if (mShaderProgram != NULL) {
        mShaderProgram->Reload();
        mShaderProgram->Use();
    }
}

void Graphics::Release() {
    ASSERT(mState == STATE_INITIALIZED);
    mShaderProgram = NULL;
    mState = STATE_RELEASED;
}

void Graphics::SetViewport(int x, int y, int width, int height) {
    ASSERT(mState == STATE_INITIALIZED || mState == STATE_FRAMED);
    ASSERT(x >= 0 && y >= 0 && width > 0 && height > 0);
    glViewport(x, y, width, height);
    CHECK_GL();
}

void Graphics::SetOrtho(float xLeft, float xRight, float yBottom, float yTop, float zNear, float zFar) {
    ASSERT(xLeft < xRight && yBottom < yTop && zNear > zFar);
    ASSERT(mState == STATE_INITIALIZED || mState == STATE_FRAMED);
    mProjectionMatrix = Matrix4(
        2 / (xRight - xLeft), 0,                    0,                   -(xRight + xLeft) / (xRight - xLeft),
        0,                    2 / (yTop - yBottom), 0,                   -(yTop + yBottom) / (yTop - yBottom),
        0,                    0,                    -2 / (zFar - zNear), -(zFar + zNear) / (zFar - zNear),
        0,                    0,                    0,                   1
    );   
}

void Graphics::Begin() {
    ASSERT(mState == STATE_INITIALIZED);
    ASSERT(mModelViewMatrixStack.Depth() == 1);
    ASSERT(mProjectionMatrixStack.Depth() == 1);
    CHECK_GL();
    while (mModelViewMatrixStack.Depth() > 1) {
        mModelViewMatrixStack.Pop();
    }
    mModelViewMatrixStack = Matrix4::Identity();
    while (mProjectionMatrixStack.Depth() > 1) {
        mProjectionMatrixStack.Pop();
    }
    mProjectionMatrixStack = mProjectionMatrix;
    mState = STATE_FRAMED;
    glClear(GL_COLOR_BUFFER_BIT);
    CHECK_GL();
}

void Graphics::End() {
    ASSERT(mState == STATE_FRAMED);
    ASSERT(mModelViewMatrixStack.Depth() == 1);
    ASSERT(mProjectionMatrixStack.Depth() == 1);
    glFlush();
    CHECK_GL();
    mState = STATE_INITIALIZED;
}

void Graphics::SetLineWidth(float lineWidth) {
    ASSERT(lineWidth > 0);
    ASSERT(mState == STATE_INITIALIZED || mState == STATE_FRAMED);
    glLineWidth(lineWidth);
    CHECK_GL();
}

void Graphics::Bind(ShaderProgram* shaderProgram) {
    ASSERT(mState == STATE_INITIALIZED || mState == STATE_FRAMED);
    ASSERT(shaderProgram != NULL);
    mShaderProgram = shaderProgram;
    mShaderProgram->Use();
}

void Graphics::DrawLines(const void* vertices, int count) {
    ASSERT(vertices != NULL && count >= 2);
    DrawArrays(GL_LINES, vertices, count);
}

void Graphics::DrawLineLoop(const void* vertices, int count) {
    ASSERT(vertices != NULL && count >= 2);
    DrawArrays(GL_LINE_LOOP, vertices, count);
}

void Graphics::DrawLineStrip(const void* vertices, int count) {
    ASSERT(vertices != NULL && count >= 2);
    DrawArrays(GL_LINE_STRIP, vertices, count);
}

void Graphics::DrawTriangleStrip(const void* vertices, int count) {
    ASSERT(vertices != NULL && count >= 3);
    DrawArrays(GL_TRIANGLE_STRIP, vertices, count);
}

void Graphics::DrawTriangleStrip(const void* vertices, const unsigned short* indices, int count) {
    ASSERT(vertices != NULL && indices != NULL && count >= 3);
    DrawElements(GL_TRIANGLE_STRIP, vertices, indices, count);
}

void Graphics::DrawTriangleFan(const void* vertices, int count) {
    ASSERT(vertices != NULL && count >= 3);
    DrawArrays(GL_TRIANGLE_FAN, vertices, count);
}

inline void Graphics::CheckChanges() {
    if (mColorStack.HasChanged()) {
        mShaderProgram->ColorChanged();
        mColorStack.Unchanged();
    }
    if (mModelViewMatrixStack.HasChanged() || mProjectionMatrixStack.HasChanged()) {
        mShaderProgram->MvpMatrixChanged();
        mModelViewMatrixStack.Unchanged();
        mProjectionMatrixStack.Unchanged();
    }
}

inline void Graphics::DrawArrays(GLenum mode, const GLvoid* vertices, GLsizei count) {
    ASSERT(mState == STATE_FRAMED && mShaderProgram != NULL);
    CheckChanges();
    mShaderProgram->SetVertexAttributes(vertices);
    glDrawArrays(mode, 0, count);
    CHECK_GL();
}

inline void Graphics::DrawElements(GLenum mode, const GLvoid* vertices, const GLvoid* indices, GLsizei count) {
    ASSERT(mState == STATE_FRAMED && mShaderProgram != NULL);
    CheckChanges();
    mShaderProgram->SetVertexAttributes(vertices);
    glDrawElements(mode, count, GL_UNSIGNED_SHORT, indices);
    CHECK_GL();
}

namespace {

Graphics graphics;

} /* anonymous namespace */

Graphics& Gfx = graphics;

} /* namespace Core */
