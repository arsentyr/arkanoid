#include "Graphics/ShaderProgram.h"

#include <vector>
#include "Assert.h"
#include "Utils/ILog.h"

namespace Core {
namespace {

GLuint CreateShader(GLenum type, const GLchar* source) {
    ASSERT(source != NULL);
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &source, NULL);
    glCompileShader(shader);
    CHECK_GL();
    GLint compileStatus = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
    CHECK_GL();
    if (!compileStatus) {
        GLint infoLogLength = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
        CHECK_GL();
        if (infoLogLength > 0) {
            GLsizei length = 0;
            std::vector<GLchar> infoLog(infoLogLength);
            glGetShaderInfoLog(shader, infoLogLength, &length, &infoLog[0]);
            CHECK_GL();
            infoLog[length] = '\0';
            Log.Error("Shader info log: %s.", &infoLog[0]);
        }
    }
    ASSERT(compileStatus);
    return shader;
}

} /* anonymous namespace */

ShaderProgram::ShaderProgram()
        : mProgram(UNKNOWN_PROGRAM)
        , mVertexShader(UNKNOWN_SHADER)
        , mFragmentShader(UNKNOWN_SHADER) {
}

ShaderProgram::~ShaderProgram() {
    ASSERT(!IsProgram());
    if (IsProgram()) {
        Delete();
    }
}

void ShaderProgram::Use() {
    glUseProgram(mProgram);
    CHECK_GL();
}

void ShaderProgram::Create(const GLchar* sourceVertexShader, const GLchar* sourceFragmentShader) {
    ASSERT(!IsProgram());
    mProgram = glCreateProgram();
    ASSERT(IsProgram());

    GLboolean hasShaderCompiler = GL_FALSE;
    glGetBooleanv(GL_SHADER_COMPILER, &hasShaderCompiler);
    ASSERT(hasShaderCompiler);

    mVertexShader = CreateShader(GL_VERTEX_SHADER, sourceVertexShader);
    mFragmentShader = CreateShader(GL_FRAGMENT_SHADER, sourceFragmentShader);
    glAttachShader(mProgram, mVertexShader);
    glAttachShader(mProgram, mFragmentShader);
    CHECK_GL();
}

void ShaderProgram::Link() {
    glLinkProgram(mProgram);
    CHECK_GL();
    GLint linkStatus = GL_FALSE;
    glGetProgramiv(mProgram, GL_LINK_STATUS, &linkStatus);
    if (!linkStatus) {
        GLint infoLogLength = 0;
        glGetProgramiv(mProgram, GL_INFO_LOG_LENGTH, &infoLogLength);
        if (infoLogLength > 0) {
            GLsizei length = 0;
            std::vector<GLchar> infoLog(infoLogLength);
            glGetProgramInfoLog(mProgram, infoLogLength, &length, &infoLog[0]);
            CHECK_GL();
            infoLog[length] = '\0';
            Log.Error("Program info log: %s.", &infoLog[0]);
        }
    }
    ASSERT(linkStatus);

    glDeleteShader(mVertexShader);
    glDeleteShader(mFragmentShader);
    CHECK_GL();
    mVertexShader = UNKNOWN_SHADER;
    mFragmentShader = UNKNOWN_SHADER;
}

void ShaderProgram::Delete() {
    glDeleteProgram(mProgram);
    CHECK_GL();
    mProgram = UNKNOWN_PROGRAM;
    mVertexShader = UNKNOWN_SHADER;
    mFragmentShader = UNKNOWN_SHADER;
}

void ShaderProgram::Lose() {
    ASSERTF(!IsProgram(), "Lost a valid program.");
    mProgram = UNKNOWN_PROGRAM;
    mVertexShader = UNKNOWN_SHADER;
    mFragmentShader = UNKNOWN_SHADER;
}

void ShaderProgram::BindAttributeLocation(GLuint index, const GLchar* name) {
    ASSERT(name != NULL);
    glBindAttribLocation(mProgram, index, name);
    CHECK_GL();
}

void ShaderProgram::SetVertexAttributePointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* pointer) {
    glVertexAttribPointer(index, size, type, normalized, stride, pointer);
    CHECK_GL();
}

void ShaderProgram::EnableVertexAttributeArray(GLuint index) {
    glEnableVertexAttribArray(index);
    CHECK_GL();
}

GLint ShaderProgram::GetUniformLocation(const GLchar* name) const {
    ASSERT(name != NULL);
    GLint location = glGetUniformLocation(mProgram, name);
    CHECK_GL();
    return location;
}

void ShaderProgram::SetUniform4fv(GLint location, GLsizei count, const GLfloat* fvalues) {
    glUniform4fv(location, count, fvalues);
    CHECK_GL();
}

void ShaderProgram::SetUniformMatrix4fv(GLint location, GLsizei count, const GLfloat* fvalues) {
    glUniformMatrix4fv(location, count, GL_FALSE, fvalues);
    CHECK_GL();
}

inline bool ShaderProgram::IsProgram() const {
    return mProgram != UNKNOWN_PROGRAM && glIsProgram(mProgram);
}

} /* namespace Core */
