#include "Graphics/MatrixStack.h"

#include "Assert.h"
#include "Math/Math.h"

namespace Core {

MatrixStack::MatrixStack(unsigned int capacity)
        : mHasChanged(true) {
    ASSERT(capacity > 0);
    mStack.reserve(capacity);
    mStack.push_back(Matrix4::Identity());
}

MatrixStack& MatrixStack::operator=(const Matrix4& matrix) {
    mStack.back() = matrix;
    mHasChanged = true;
    return *this;
}

MatrixStack::operator const Matrix4& () const {
    return mStack.back();
}

void MatrixStack::Push() {
    ASSERT(mStack.size() < mStack.capacity());
    mStack.push_back(mStack.back()); 
    mHasChanged = true;
}

void MatrixStack::Pop() {
    VERIFY(mStack.size() > 1, "Matrix stack underflow.");
    mStack.pop_back();
    mHasChanged = true;
}

const Matrix4& MatrixStack::Top() const {
    return mStack.back();
}

unsigned int MatrixStack::Depth() const {
    return mStack.size();
}

void MatrixStack::Scale(float scaleX, float scaleY, float scaleZ) {
    Matrix4& top = mStack.back();
    top.x0 *= scaleX; top.x1 *= scaleX; top.x2 *= scaleX; top.x3 *= scaleX;
    top.y0 *= scaleY; top.y1 *= scaleY; top.y2 *= scaleY; top.y3 *= scaleY;
    top.z0 *= scaleZ; top.z1 *= scaleZ; top.z2 *= scaleZ; top.z3 *= scaleZ;
    mHasChanged = true;
}

void MatrixStack::Translate(float deltaX, float deltaY, float deltaZ) {
    Matrix4& top = mStack.back();
    top.w0 += deltaX * top.x0 + deltaY * top.y0 + deltaZ * top.z0;
    top.w1 += deltaX * top.x1 + deltaY * top.y1 + deltaZ * top.z1;
    top.w2 += deltaX * top.x2 + deltaY * top.y2 + deltaZ * top.z2;
    top.w3 += deltaX * top.x3 + deltaY * top.y3 + deltaZ * top.z3;
    mHasChanged = true;
}

void MatrixStack::RotateZ(float degrees) {
    const float radians = DegToRad(degrees);
    const float s = Sin(radians);
    const float c = Cos(radians);
    Matrix4& top = mStack.back();
    const float x0 = c * top.x0 + s * top.y0;
    const float x1 = c * top.x1 + s * top.y1;
    const float x2 = c * top.x2 + s * top.y2;
    const float x3 = c * top.x3 + s * top.y3;
    top.y0 = c * top.y0 - s * top.x0;
    top.y1 = c * top.y1 - s * top.x1;
    top.y2 = c * top.y2 - s * top.x2;
    top.y3 = c * top.y3 - s * top.x3;
    top.x0 = x0;
    top.x1 = x1;
    top.x2 = x2;
    top.x3 = x3;
    mHasChanged = true;
}

bool MatrixStack::HasChanged() const {
    return mHasChanged;
}

void MatrixStack::Unchanged() {
    mHasChanged = false;
}

} /* namespace Core */
