#include "Graphics/ColorStack.h"

#include "Assert.h"

namespace Core {

ColorStack::ColorStack(unsigned int capacity)
        : mHasChanged(true) {
    ASSERT(capacity > 0);
    mStack.reserve(capacity);
    Push(Color::White());
}

ColorStack& ColorStack::operator=(const Color& color) {
    mStack.back() = color;
    mHasChanged = true;
    return *this;
}

ColorStack::operator const Color& () const {
    return mStack.back();
}

void ColorStack::Push(float red, float green, float blue, float alpha) {
    ASSERT(mStack.size() < mStack.capacity());
    mStack.push_back(Color(red, green, blue, alpha));
    CHECK_COLOR(mStack.back());
    mHasChanged = true;
}

void ColorStack::Push(const Color& color) {
    ASSERT(mStack.size() < mStack.capacity());
    CHECK_COLOR(color);
    mStack.push_back(color);
    mHasChanged = true;
}

void ColorStack::Pop() {
    VERIFY(mStack.size() > 1, "Color stack underflow.");
    mStack.pop_back();
    mHasChanged = true;
}

const Color& ColorStack::Top() const {
    return mStack.back();
}

unsigned int ColorStack::Depth() const {
    return mStack.size();
}

bool ColorStack::HasChanged() const {
    return mHasChanged;
}

void ColorStack::Unchanged() {
    mHasChanged = false;
}

} /* namespace Core */
