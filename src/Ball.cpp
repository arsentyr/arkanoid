#include "Ball.h"

#include "Vertex.h"

namespace Arkanoid {

Vector::Vector(float x, float y)
        : x(x)
        , y(y) {
}

Vector::Vector(const Vector& vec)
        : x(vec.x)
        , y(vec.y) {
}

Vector Vector::operator*(float scalar) const {
    return Vector(x * scalar, y * scalar);
}

Vector Vector::operator+(const Vector& vec) const {
    return Vector(x + vec.x, y + vec.y);
}

float Vector::Length() const {
    return Sqrt(x * x + y * y);
}

Vector& Vector::Normalize() {
	const float length = Length();
	x /= length;
	y /= length;
    return *this;
}

Ball::Ball(float x, float y, float velocity)
        : x(x)
        , y(y)
		, velocity(Rand.NextBool() ? -Rand.NextInt(3, 6) / 10.f : Rand.NextInt(3, 6) / 10.f, 1)
        , state(BALL_BOUNCING) {
    SetVelocity(velocity);
}

float Ball::Left() const {
    return x - Radius(); 
}

float Ball::Right() const {
    return x + Radius();
}

float Ball::Top() const {
    return y + Radius();
}

float Ball::Bottom() const {
    return y - Radius();
}

void Ball::SetVelocity(float xy) {
    velocity = velocity.Normalize() * xy;
}

void Ball::InverseVelocityX() {
    velocity.x = -velocity.x;
}

void Ball::InverseVelocityY() {
    velocity.y = -velocity.y;
}

void Ball::Draw() {
    static const Color color(32 / 255.f, 170 / 255.f, 214 / 255.f, 92 / 255.f);
    static const Vertex ball[] = {
        Vertex(0, 0, color),
        Vertex(-0.5, -1, color),
        Vertex(-1, 0, color),
        Vertex(-0.5, 1, color),
        Vertex(0.5, 1, color),
        Vertex(1, 0, color),
        Vertex(0.5, -1, color),
        Vertex(-0.5, -1, color)
    };
    static const Vertex border[] = {
        Vertex(-0.5, -1, color),
        Vertex(-1, 0, color),
        Vertex(-0.5, 1, color),
        Vertex(0.5, 1, color),
        Vertex(1, 0, color),
        Vertex(0.5, -1, color)
    };

    Gfx.ModelView().Push();
    Gfx.ModelView().Translate(x, y);
    Gfx.ModelView().Scale(Radius(), Radius());
    Gfx.DrawTriangleFan(&ball[0], 8);
    Gfx.BlendMode().Push(BLEND_ADD);
    Gfx.SetLineWidth(3);
    Gfx.DrawLineLoop(&border[0], 6);
    Gfx.SetLineWidth(2);
    Gfx.DrawLineLoop(&border[0], 6);
    Gfx.SetLineWidth(1);
    Gfx.DrawLineLoop(&border[0], 6);
    Gfx.BlendMode().Pop();
    Gfx.ModelView().Pop();
}

float Ball::Radius() {
    return LOGICAL_WIDTH * 0.015f;
}

} /* namespace Arkanoid */
