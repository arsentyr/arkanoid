#include "Application.h"

namespace Arkanoid {

Application& Application::Instance() {
    static Application application;
    return application;
}

Application::Application()
        : mScale(1)
        , mHalfWidth(0)
        , mHalfHeight(0)
        , mState(STATE_UNITIALIZED) {
}

void Application::Init() {
    ASSERT(mState == STATE_UNITIALIZED);
    if (mState != STATE_UNITIALIZED) {
        Log.Error("Init application failed: application already initialized.");
        return;
    }

    Log.Debug("Initialize.");
    Gfx.Init();
    mShaderProgram.Load();
    Gfx.Bind(&mShaderProgram);
    mState = STATE_INITIALIZED;
}

void Application::Reload() {
    ASSERT(mState != STATE_UNITIALIZED && mState != STATE_RELEASED);
    if (mState == STATE_UNITIALIZED || mState == STATE_RELEASED) {
        Log.Error("Reload application failed: application not initialized.");
        return;
    }

    Log.Debug("Reload.");
    Gfx.Reload();
    mShaderProgram.Reload();
}

void Application::Resize(int width, int height) {
    ASSERT(mState != STATE_UNITIALIZED && mState != STATE_RELEASED);
    ASSERT(width > 0 && height > 0 && width <= height);
    if (mState == STATE_UNITIALIZED || mState == STATE_RELEASED) {
        Log.Error("Resize application failed: application not initialized.");
        return;
    }

    Log.Debug("Resize: width=%d, height=%d.", width, height);
	mScale = Min((width / LOGICAL_WIDTH), (height / LOGICAL_HEIGHT));
    mHalfWidth = width / 2.f;
    mHalfHeight = height / 2.f;
    Gfx.SetViewport(0, 0, width, height);
    Gfx.SetOrtho(-mHalfWidth, mHalfWidth, -mHalfHeight, mHalfHeight, 1, -1);
}
 
void Application::Resume() {
    ASSERT(mState == STATE_INITIALIZED || mState == STATE_PAUSED);
    if (mState != STATE_INITIALIZED && mState != STATE_PAUSED) {
        Log.Error("Resume application failed: incorrect state=%d.", mState);
        return;
    }

    Log.Debug("Resume.");
    mTimer.Start();
    mState = STATE_RESUMED;
}

void Application::Loop() {
    ASSERT(mState == STATE_RESUMED);
    if (mState != STATE_RESUMED) {
        Log.Error("Application looping failed: application not resumed, incorrect state=%d.", mState);
        return;
    }

    const float MAX_DELTA_TIME = 0.5f;
    float deltaTime = mTimer.GetDeltaTime();
    ASSERT(deltaTime >= 0);
    if (deltaTime > MAX_DELTA_TIME) {
        Log.Warning("Delta time=%f > max delta time=%f.", deltaTime, MAX_DELTA_TIME);
        deltaTime = MAX_DELTA_TIME;
    }

    mLevel.Update(deltaTime);
    Gfx.Begin();
    Gfx.Projection().Scale(mScale, mScale);
	Gfx.Projection().Translate((LOGICAL_WIDTH / -2.f), (LOGICAL_HEIGHT / -2.f));
    mLevel.Draw();
    Gfx.End();

    if (Config::DEBUG) {
        const float FPS_DURATION = 180;
        static int frames = 0;
        static float seconds = 0;
        ++frames;
        seconds += deltaTime;
        if (seconds >= FPS_DURATION) {
            Log.Debug("FPS: %d.", static_cast<int>(frames / seconds));
            frames = 0;
            seconds = 0;
        }
    }
}

void Application::Pause() {
    ASSERT(mState == STATE_INITIALIZED || mState == STATE_RESUMED);
    if (mState != STATE_INITIALIZED && mState != STATE_RESUMED) {
        Log.Error("Pause application failed: incorrect state=%d.", mState);
        return;
    }

    Log.Debug("Pause.");
    mTimer.Stop();
    mState = STATE_PAUSED;
} 

void Application::Release() {
    ASSERT(mState != STATE_UNITIALIZED && mState != STATE_RELEASED);
    if (mState == STATE_UNITIALIZED || mState == STATE_RELEASED) {
        Log.Error("Release application failed: application not initialized, incorrect state=%d.", mState);
        return;
    }
    if (mState != STATE_PAUSED) {
        Log.Error("Application not paused.");
        Pause();
    }

    Log.Debug("Release.");
    mShaderProgram.Unload();
    Gfx.Release();
    mState = STATE_RELEASED;
}

void Application::TouchDown(float x, float y) {
    ASSERT(mState == STATE_RESUMED);
    if (mState == STATE_RESUMED) {
        TransformTouch(x, y);
        mLevel.TouchDown(x, y);
    }
}

void Application::TouchMove(float x, float y) {
    ASSERT(mState == STATE_RESUMED);
    if (mState == STATE_RESUMED) {
        TransformTouch(x, y);
        mLevel.TouchMove(x, y);
    }
}

void Application::TouchUp(float x, float y) {
    ASSERT(mState == STATE_RESUMED);
    if (mState == STATE_RESUMED) {
        TransformTouch(x, y);
        mLevel.TouchUp(x, y);
    }
}

inline void Application::TransformTouch(float& x, float& y) {
    x = x - mHalfWidth;
    y = mHalfHeight - y;
    x /= mScale;
    y /= mScale;
	x += LOGICAL_WIDTH / 2.f;
	y += LOGICAL_HEIGHT / 2.f;
}

} /* namespace Arkanoid */
