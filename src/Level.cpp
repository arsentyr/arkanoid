#include "Level.h"

#include "Core.h"

namespace Arkanoid {

inline float Lerp(float a, float b, float t) {
    return a + (b - a) * t;
}

Level::Level()
        : mNumChips(0)
        , mNumCatch(0)
        , mHasSlow(false)
        , mSlowTime(0)
        , mTouchX(0) {
    Reset();
}

void Level::ResetRacket() {
    const float width = LOGICAL_WIDTH * 0.15f;
    const float height = LOGICAL_HEIGHT * 0.02f;
    const float x = LOGICAL_WIDTH / 2.f - width / 2.f;
    const float y = LOGICAL_HEIGHT * 0.125f;
    mRacket.SetLocation(x, y, width, height);
}

void Level::ResetBalls() {
    mBalls.clear();
    const float x = Lerp(mRacket.Left(), mRacket.Right(), 0.5f);
    const float y = mRacket.Top() + Ball::Radius();
    mBalls.push_back(Ball(x, y, BALL_MAX_VELOCITY));
}

void Level::ResetChips() {
    static bool first = false;
    first = !first;
    const char* level = first ? &levelA[0][0] : &levelB[0][0];

    enum {
        COLOR_GREEN_MASK = 1,
        COLOR_PINK_MASK = 2,
        COLOR_PURPLE_MASK = 3,
        COLOR_ORANGE_MASK = 4,
        COLOR_YELLOW_MASK = 5,
        COLOR_RED_MASK = 6,
        COLOR_MASK = 7,
        BONUS_CATCH_MASK = 8,
        BONUS_DIVIDE_MASK = 16,
        BONUS_SLOW_MASK = 32,
        BONUS_MASK = BONUS_CATCH_MASK + BONUS_DIVIDE_MASK + BONUS_SLOW_MASK
    };

    const Color electricLime(29 / 255.f, 249 / 255.f, 20 / 255.f, 104 / 255.f);
    const Color shockingPink(252 / 255.f, 126 / 255.f, 253 / 255.f, 104 / 255.f);
    const Color purpleHeart(116 / 255.f, 66 / 255.f,  216 / 255.f, 104 / 255.f);
    const Color burntOrange(255 / 255.f, 127 / 255.f, 73 / 255.f,  104 / 255.f);
    const Color laserLemon(253 / 255.f, 252 / 255.f, 116 / 255.f, 104 / 255.f);
    const Color radicalRed(255 / 255.f, 73 / 255.f, 108 / 255.f, 104 / 255.f);
	Color color;
    BonusType bonus;
    mNumChips = 0;
    for (int row = 0; row < GRID_NUM_ROWS; ++row) {
        for (int col = 0; col < GRID_NUM_COLS; ++col) {
            const char value = *(level + (row * GRID_NUM_COLS) + col);
            if (value > 0) {
                const float x = col * CELL_WIDTH;
                const float y = (GRID_NUM_ROWS - row) * CELL_HEIGHT - CELL_HEIGHT;
                
                switch (value & COLOR_MASK) {
                case COLOR_GREEN_MASK: color = electricLime; break;
                case COLOR_PINK_MASK: color = shockingPink; break;
                case COLOR_PURPLE_MASK: color = purpleHeart; break;
                case COLOR_ORANGE_MASK: color = burntOrange; break;
                case COLOR_YELLOW_MASK: color = laserLemon; break; 
                case COLOR_RED_MASK: color = radicalRed; break;
                }

                switch (value & BONUS_MASK) {
                case BONUS_CATCH_MASK: bonus = BONUS_CATCH; break;
                case BONUS_DIVIDE_MASK: bonus = BONUS_DIVIDE; break;
                case BONUS_SLOW_MASK: bonus = BONUS_SLOW; break;
                default: bonus = BONUS_NONE; break;
                }

                mChips[row][col] = new Chip(x, y, CELL_WIDTH, CELL_HEIGHT, color, bonus);
                ++mNumChips;
            }
            else {
                mChips[row][col] = NULL;
            }
        }
    }
}

void Level::Reset() {
    ResetRacket();
    ResetBalls();
    ResetChips();
    mNumCatch = 0;
    mHasSlow = false;
    mSlowTime = 0;
    mBonuses.clear();
    mTouchX = mRacket.Left();
}

void Level::TouchDown(float x, float /*y*/) {
    mTouchX = x;
}

void Level::TouchMove(float x, float /*y*/) {
    float dx = x - mTouchX;
    if (mRacket.Left() + dx < 0) {
        dx = -mRacket.Left();
    }
    else if (mRacket.Right() + dx > LOGICAL_WIDTH) {
        dx = LOGICAL_WIDTH - mRacket.Right();
    }
    mRacket.SetOffsetX(dx);

    for (Balls::iterator it = mBalls.begin(); it != mBalls.end(); ++it) {
		if (it->state == BALL_BOUNCING || it->state == BALL_CATCHING) {
            it->x += dx;
        }
    }

    mTouchX = x;
}

void Level::TouchUp(float /*x*/, float /*y*/) {
    for (Balls::iterator it = mBalls.begin(); it != mBalls.end(); ++it) {
        switch (it->state) {
        case BALL_BOUNCING: it->state = BALL_BOUNCE; break;
        case BALL_CATCHING: it->state = BALL_CATCH; break;
        }
    }
}

bool Level::CheckChip(int row, int col) {
    row = GRID_NUM_ROWS - row - 1;
    if (mChips[row][col] != NULL) {
        Chip* chip = mChips[row][col];
        switch (chip->Bonus()) {
        case BONUS_CATCH:
        case BONUS_DIVIDE:
        case BONUS_SLOW:
            mBonuses.push_back(Bonus(Lerp(chip->Left(), chip->Right(), 0.5f), chip->Bottom(), chip->Bonus()));
            break;
        }
        delete chip;
        mChips[row][col] = NULL;
        --mNumChips;
        return true;
    }
    return false;
}

void Level::Update(float deltaTime) {
    Bonuses::iterator bonus = mBonuses.begin();
    while (bonus != mBonuses.end()) {
        bonus->Update(deltaTime);
        if (mRacket.Hit(*bonus)) {
            switch (bonus->Type()) {
            case BONUS_CATCH:
                for (Balls::iterator ball = mBalls.begin(); ball != mBalls.end(); ++ball) {
                    ball->state = BALL_CATCH;
                }
                mNumCatch += NUM_CATCH;
                break;
            case BONUS_DIVIDE:
                mBalls.push_back(Ball(Lerp(mRacket.Left(), mRacket.Right(), 0.5f), mRacket.Top() + Ball::Radius() + 1, BALL_MAX_VELOCITY));
                break;
            case BONUS_SLOW:
                mHasSlow = true;
                mSlowTime += SLOW_TIME;
                for (Balls::iterator it = mBalls.begin(); it != mBalls.end(); ++it) {
                    it->SetVelocity(BALL_MIN_VELOCITY);
                }
                break;
            }
            bonus = mBonuses.erase(bonus);
        }
        else if (bonus->Top() < 0) {
            bonus = mBonuses.erase(bonus);
        }
        else {
            ++bonus;
        }
    }

    if (mHasSlow) {
        mSlowTime -= deltaTime;
        if (mSlowTime <= 0) {
            mHasSlow = false;
            mSlowTime = 0;
            for (Balls::iterator ball = mBalls.begin(); ball != mBalls.end(); ++ball) {
                ball->SetVelocity(BALL_MAX_VELOCITY);
            }
        }
    }

    Balls::iterator it = mBalls.begin();
    while (it != mBalls.end()) {
        Ball& ball = *it;
        if (ball.state != BALL_BOUNCING && ball.state != BALL_CATCHING) {
            const Vector before(ball.x, ball.y);
            const Vector after(before + ball.velocity * deltaTime);

            if (after.x - ball.Radius() <= 0) {
                ball.x = ball.Radius();
                ball.InverseVelocityX();
                continue;;
            }
            else if (after.x + ball.Radius() >= LOGICAL_WIDTH) {
                ball.x = LOGICAL_WIDTH - ball.Radius() - 1;
                ball.InverseVelocityX();
                continue;
            }
            if (after.y + ball.Radius() <= 0) {
                it = mBalls.erase(it);
                if (mBalls.empty()) {
                    ResetBalls();
                    break;
                }
                continue;
            }
            else if (after.y + ball.Radius() >= LOGICAL_HEIGHT) {
                ball.y = LOGICAL_HEIGHT - ball.Radius() - 1;
                ball.InverseVelocityY();
                continue;
            }

            for (int step = 1; step <= 5; ++step) {
                ball.x = Lerp(before.x, after.x, step * 0.2f);
                ball.y = Lerp(before.y, after.y, step * 0.2f);
                if (mRacket.Hit(ball)) {
                    if (ball.x < mRacket.Left()) {
                        if (ball.y > mRacket.Top()) {
                            ball.InverseVelocityX();
                            ball.InverseVelocityY();
                            ball.x = mRacket.Left() - ball.Radius();
                            ball.y = mRacket.Top() + ball.Radius();
                        }
                        else {
                            ball.InverseVelocityX();
                            ball.x = mRacket.Left() - ball.Radius() - 1;
                        }
                    }
                    else if (ball.x > mRacket.Right()) {
                        if (ball.y > mRacket.Top()) {
                            ball.InverseVelocityX();
                            ball.InverseVelocityY();
                            ball.x = mRacket.Right() + ball.Radius();
                            ball.y = mRacket.Top() + ball.Radius();
                        }
                        else {
                            ball.InverseVelocityX();
                            ball.x = mRacket.Right() + ball.Radius() + 1;
                        }
                    }
                    else {
                        ball.InverseVelocityY();
                        ball.y = mRacket.Top() + ball.Radius() + 1;
                        if (ball.state == BALL_CATCH) {
                            if (mNumCatch > 0) {
                                --mNumCatch;
                            }
                            ball.state = (mNumCatch > 0) ? BALL_CATCHING : BALL_BOUNCE;
                        }
                    }
                    break;
                }
                else if (CheckChip((ball.y + 0.7071 * ball.Radius()) / CELL_HEIGHT, (ball.x - 0.7071 * ball.Radius()) / CELL_WIDTH)
                        || CheckChip((ball.y + 0.7071 * ball.Radius()) / CELL_HEIGHT, (ball.x + 0.7071 * ball.Radius()) / CELL_WIDTH)
                        || CheckChip((ball.y - 0.7071 * ball.Radius()) / CELL_HEIGHT, (ball.x - 0.7071 * ball.Radius()) / CELL_WIDTH)
                        || CheckChip((ball.y - 0.7071 * ball.Radius()) / CELL_HEIGHT, (ball.x + 0.7071 * ball.Radius()) / CELL_WIDTH)) {
                    ball.InverseVelocityX();
                    ball.InverseVelocityY();
                    break;
                }
                else if (CheckChip(ball.Top() / CELL_HEIGHT, ball.x / CELL_WIDTH)
                        || CheckChip(ball.Bottom() / CELL_HEIGHT, ball.x / CELL_WIDTH)) {
                    ball.InverseVelocityY();
                    break;
                }
                else if (CheckChip(ball.y / CELL_HEIGHT, ball.Left() / CELL_WIDTH)
                        || CheckChip(ball.y / CELL_HEIGHT, ball.Right() / CELL_WIDTH)) {
                    ball.InverseVelocityX();
                    break;
                }
            }
        }
        ++it;
    }

    if (mNumChips == 0) {
        Reset();
    }
}

void Level::Draw() {
    static const Color backgroundColor(1, 0, 1, 32 / 255.f);
    static const Vertex background[] = {
        Vertex(0, 0, backgroundColor),
        Vertex(0, LOGICAL_HEIGHT, backgroundColor),
        Vertex(LOGICAL_WIDTH, 0, backgroundColor),
        Vertex(LOGICAL_WIDTH, LOGICAL_HEIGHT, backgroundColor)
    };
    Gfx.DrawTriangleStrip(&background[0], 4);

    for (int row = 0; row < GRID_NUM_ROWS / 2; ++row) {
        for (int col = 0; col < GRID_NUM_COLS; ++col) {
            if (mChips[row][col] != NULL) {
                mChips[row][col]->Draw();
            }
        }
    }
    for (Bonuses::iterator it = mBonuses.begin(); it != mBonuses.end(); ++it) {
        it->Draw();
    }
    for (Balls::iterator it = mBalls.begin(); it != mBalls.end(); ++it) {
        it->Draw();
    }
    mRacket.Draw();
}

} /* namespace Arkanoid */
