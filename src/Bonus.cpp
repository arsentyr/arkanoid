#include "Bonus.h"

#include "Core.h"
#include "Vertex.h"

namespace Arkanoid {

float Bonus::sWidth = LOGICAL_WIDTH * 0.03f;
float Bonus::sHeight = LOGICAL_HEIGHT * 0.0275f;

Bonus::Bonus(float x, float y, BonusType type)
        : mX(x)
        , mY(y)
        , mType(type) {
}

float Bonus::Left() const {
    return mX - sWidth / 2;
}

float Bonus::Right() const {
    return mX + sWidth / 2;
}

float Bonus::Top() const {
    return mY + sHeight / 2;
}

float Bonus::Bottom() const {
    return mY - sHeight / 2;
}

BonusType Bonus::Type() const {
    return mType;
}

void Bonus::Update(float deltaTime) {
    mY -= LOGICAL_HEIGHT * 0.15f * deltaTime;
}

void Bonus::Draw() {
    static const Color catchColor(29 / 255.f, 249 / 255.f, 20 / 255.f, 128 / 255.f);
    static const Vertex catchBackground[] = {
        Vertex(-0.5, -0.5, catchColor),
        Vertex(-0.5, 0.5, catchColor),
        Vertex(0.5, -0.5, catchColor),
        Vertex(0.5, 0.5, catchColor)
    };
    static const Vertex catchBorder[] = {
        Vertex(0.5, -0.5, catchColor),
        Vertex(-0.5, -0.5, catchColor),
        Vertex(-0.5, 0.5, catchColor),
        Vertex(0.5, 0.5, catchColor)
    };

    static const Color divideColor(32 / 255.f, 170 / 255.f, 214 / 255.f, 92 / 255.f);
    static const Vertex divideBackground[] = {
        Vertex(-0.5, -0.5, divideColor),
        Vertex(-0.5, 0.5, divideColor),
        Vertex(0.5, -0.5, divideColor),
        Vertex(0.5, 0.5, divideColor)
    };
    static const Vertex divideBorder[] = {
        Vertex(-0.5, -0.5, divideColor),
        Vertex(0.5, 0.0, divideColor),
        Vertex(-0.5, 0.5, divideColor)
    };

    static const Color slowColor(252 / 255.f, 126 / 255.f, 253 / 255.f, 104 / 255.f);
    static const Vertex slowBackground[] = {
        Vertex(-0.5, -0.5, slowColor),
        Vertex(-0.5, 0.5, slowColor),
        Vertex(0.5, -0.5, slowColor),
        Vertex(0.5, 0.5, slowColor)
    };
    static const Vertex slowBorder[] = {
        Vertex(-0.5, -0.5, slowColor),
        Vertex(0.5, -0.5, slowColor),
        Vertex(0.5, 0.0, slowColor),
        Vertex(-0.5, 0.0, slowColor),
        Vertex(-0.5, 0.5, slowColor),
        Vertex(0.5, 0.5, slowColor)
    };

    Gfx.ModelView().Push();
    Gfx.ModelView().Translate(mX, mY);
    Gfx.ModelView().Scale(sWidth, sHeight);
    switch (mType) {
    case BONUS_CATCH:
        Gfx.DrawTriangleStrip(&catchBackground[0], 4);
        Gfx.BlendMode().Push(BLEND_ADD);
        Gfx.SetLineWidth(3);
        Gfx.DrawLineStrip(&catchBorder[0], 4);
        Gfx.SetLineWidth(2);
        Gfx.DrawLineStrip(&catchBorder[0], 4);
        Gfx.SetLineWidth(1);
        Gfx.DrawLineStrip(&catchBorder[0], 4);
        Gfx.BlendMode().Pop();
        break;
    case BONUS_DIVIDE:
        Gfx.DrawTriangleStrip(&divideBackground[0], 4);
        Gfx.BlendMode().Push(BLEND_ADD);
        Gfx.SetLineWidth(3);
        Gfx.DrawLineLoop(&divideBorder[0], 3);
        Gfx.SetLineWidth(2);
        Gfx.DrawLineLoop(&divideBorder[0], 3);
        Gfx.SetLineWidth(1);
        Gfx.DrawLineLoop(&divideBorder[0], 3);
        Gfx.BlendMode().Pop();
        break;
    case BONUS_SLOW:
        Gfx.DrawTriangleStrip(&slowBackground[0], 4);
        Gfx.BlendMode().Push(BLEND_ADD);
        Gfx.SetLineWidth(3);
        Gfx.DrawLineStrip(&slowBorder[0], 6);
        Gfx.SetLineWidth(2);
        Gfx.DrawLineStrip(&slowBorder[0], 6);
        Gfx.SetLineWidth(1);
        Gfx.DrawLineStrip(&slowBorder[0], 6);
        Gfx.BlendMode().Pop();
        break;
    }
    Gfx.ModelView().Pop();
}

} /* namespace Arkanoid */
