#ifndef CORE_ASSERT_H
#define CORE_ASSERT_H

#include <cstdio>
#include <cstdlib>

#ifdef NDEBUG
#   define ASSERT(...) ((void)0)
#   define ASSERTF(...) ((void)0)
#else
#   define ASSERT(expr) do { if (!(expr)) { fprintf(stderr, "Assertion failed: %s, file %s, line %d. \n", #expr, __FILE__, __LINE__); abort(); } } while (false)
#   define ASSERTF(expr, format, ...) do { if (!(expr)) { fprintf(stderr, "ASSERT: " format "\n", ##__VA_ARGS__); ASSERT(expr); } } while (false)
#endif /* NDEBUG */

#define VERIFY(expr, format, ...) do { if (!(expr)) { fprintf(stderr, "VERIFY: " format "\n", ##__VA_ARGS__); abort(); } } while (false)

#endif /* CORE_ASSERT_H */
