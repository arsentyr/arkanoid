package arsentyr.arkanoid;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class GameSurfaceView extends GLSurfaceView {
    
    public GameSurfaceView(Context context) {
        super(context);
        initialize();
    }
    
    public GameSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }
    
    protected void initialize() {
        getHolder().setFormat(PixelFormat.RGBA_8888);

        setFocusableInTouchMode(true);
        setEGLContextClientVersion(2);
        setEGLConfigChooser(8, 8, 8, 8, 0, 0);
        setPreserveEGLContextOnPause(true);

        final Context context = getContext();
        mNativeEngine = new NativeEngine(context);
        setRenderer(mNativeEngine);
    }

    @Override
    public void onResume() {
        super.onResume();
        queueEvent(new Runnable() {
            public void run() {
                mNativeEngine.onResume();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        queueEvent(new Runnable() {
            public void run() {
                mNativeEngine.onPause();
            }
        });
    }

    @Override
    public void onWindowFocusChanged(final boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        queueEvent(new Runnable() {
            public void run() {
                mNativeEngine.onWindowFocusChanged(hasWindowFocus);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
        case KeyEvent.KEYCODE_BACK:
            queueEvent(new Runnable() {
                public void run() {
                    mNativeEngine.onFinish();
                    final Activity activity = (Activity) getContext();
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            activity.finish();
                        }
                    });
                }
            });
            break;
        }
        return true;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return true;
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        queueEvent(new Runnable() {
            public void run() {
                mNativeEngine.onTouchEvent(event);
            }
        });
        return true;
    }

    private static class NativeEngine implements GLSurfaceView.Renderer {

        public NativeEngine(Context context) {
            mContext = context;
        }

        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            if (mIsSurfaceCreated) {
                checkPausing();
                nativeReload();
            } else {
                mIsSurfaceCreated = true;
                nativeInit();
            }
            mIsSurfaceChanged = false;
        }
                    
        public void onSurfaceChanged(GL10 gl, int width, int height) {
            mIsSurfaceChanged = (width > 0 && height > 0 && width <= height);
            if (mIsSurfaceChanged) {
               nativeResize(width, height);
            }
            checkResuming();
        }
            
        public void onDrawFrame(GL10 gl) {
            if (mIsResumed) {
                nativeLoop();
            }
        }

        public void onTouchEvent(MotionEvent event) {
            if (mIsResumed) {
                switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    if (mHasTouch) {
                        nativeTouchUp(event.getX(), event.getY()); 
                    }
                    mHasTouch = true;
                    mPointerId = event.getPointerId(0);
                    nativeTouchDown(event.getX(), event.getY());
                    break;
                case MotionEvent.ACTION_UP:
                    if (mHasTouch && mPointerId == event.getPointerId(0)) {
                        nativeTouchUp(event.getX(), event.getY());
                        mHasTouch = false;
                    }
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    if (mHasTouch) {
                        final int index = event.getActionIndex();
                        if (mPointerId == event.getPointerId(index)) {
                            nativeTouchUp(event.getX(index), event.getY(index));
                            mHasTouch = false;
                        }
                    }
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mHasTouch) {
                        final int index = event.findPointerIndex(mPointerId);
                        if (index >= 0) {
                            nativeTouchMove(event.getX(index), event.getY(index));
                        }
                    }
                    else {
                        mHasTouch = true;
                        mPointerId = event.getPointerId(0);
                        nativeTouchDown(event.getX(), event.getY()); 
                    }
                    break;
                case MotionEvent.ACTION_CANCEL:
                    if (mHasTouch) {
                        final int index = event.findPointerIndex(mPointerId);
                        if (index >= 0) {
                            nativeTouchUp(event.getX(index), event.getY(index));
                        }
                    }
                    break;
                }
            }
        }

        public void onWindowFocusChanged(boolean hasWindowFocus) {
            mHasFocus = hasWindowFocus;
            if (mHasFocus) {
                checkResuming();
            } else {
                checkPausing();
            }
        }

        public void onResume() {
            mHasResume = true;
            checkResuming();
        }

        public void onPause() {
            mHasResume = false;
            checkPausing();
        }

        public void onFinish() {
            mIsSurfaceCreated = false;
            mIsSurfaceChanged = false;
            checkPausing();
            nativeRelease();
        }

        private void checkResuming() {
            if (!mIsResumed
                    && mIsSurfaceChanged && mHasResume && mHasFocus) {
                mIsResumed = true;
                nativeResume();
            }
        }

        private void checkPausing() {
            if (mIsResumed) {
                mIsResumed = false;
                nativePause();
            }
        }

        private static native void nativeInit();
        private static native void nativeReload();
        private static native void nativeResize(int width, int height);
        private static native void nativeResume();
        private static native void nativeLoop();
        private static native void nativePause();
        private static native void nativeRelease();
        private static native void nativeTouchDown(float x, float y);
        private static native void nativeTouchMove(float x, float y);
        private static native void nativeTouchUp(float x, float y);

        private Context mContext;
        private boolean mHasResume = false;
        private boolean mHasFocus = false;
        private boolean mIsSurfaceCreated = false;
        private boolean mIsSurfaceChanged = false;
        private boolean mIsResumed = false;
        private boolean mHasTouch = false;
        private int mPointerId;
    }

    private NativeEngine mNativeEngine;

    static {
        System.loadLibrary("arkanoid");
    }
}
