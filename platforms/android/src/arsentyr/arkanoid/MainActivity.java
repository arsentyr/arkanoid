package arsentyr.arkanoid;

import android.app.Activity;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Process;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        mView = (GameSurfaceView) findViewById(R.id.game_surface_view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        final int pid = Process.myPid();
        Process.killProcess(pid);
    }

    private GameSurfaceView mView;
}
