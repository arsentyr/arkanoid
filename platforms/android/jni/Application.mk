APP_ABI := armeabi
APP_PLATFORM := android-9
APP_MODULES := arkanoid
APP_CPPFLAGS := -std=c++11 -Wfatal-errors -Wall -Wextra #-Weffc++ -pedantic
APP_STL := gnustl_static
NDK_TOOLCHAIN_VERSION := clang3.3
