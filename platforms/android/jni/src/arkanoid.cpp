#include <jni.h>
#include "Application.h"

namespace {

using App = Arkanoid::Application;

void Init(JNIEnv* /*env*/, jclass /*clazz*/) {
    App::Instance().Init();
}

void Reload(JNIEnv* /*env*/, jclass /*clazz*/) {
    App::Instance().Reload();
}

void Resize(JNIEnv* /*env*/, jclass /*clazz*/, jint width, jint height) {
    App::Instance().Resize(width, height);
}

void Resume(JNIEnv* /*env*/, jclass /*clazz*/) {
    App::Instance().Resume();
}

void Loop(JNIEnv* /*env*/, jclass /*clazz*/) {
    App::Instance().Loop();
}

void Pause(JNIEnv* /*env*/, jclass /*clazz*/) {
    App::Instance().Pause();
}

void Release(JNIEnv* /*env*/, jclass /*clazz*/) {
    App::Instance().Release();
}

void TouchDown(JNIEnv* /*env*/, jclass /*clazz*/, jfloat x, jfloat y) {
    App::Instance().TouchDown(x, y);
}

void TouchMove(JNIEnv* /*env*/, jclass /*clazz*/, jfloat x, jfloat y) {
    App::Instance().TouchMove(x, y);
}

void TouchUp(JNIEnv* /*env*/, jclass /*clazz*/, jfloat x, jfloat y) {
    App::Instance().TouchUp(x, y);
}

} /* anonymouse namespace */

extern "C"
jint JNI_OnLoad(JavaVM* vm, void* /*reserved*/) {
    JNIEnv* env = NULL;
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return JNI_ERR;
    }

    const JNINativeMethod methods[] = {
        {
            "nativeInit",
            "()V",
            reinterpret_cast<void*>(Init)
        },
        {
            "nativeReload",
            "()V",
            reinterpret_cast<void*>(Reload)
        },
        {
            "nativeResize",
            "(II)V",
            reinterpret_cast<void*>(Resize)
        },
        {
            "nativeResume",
            "()V",
            reinterpret_cast<void*>(Resume)
        },
        {
            "nativeLoop",
            "()V",
            reinterpret_cast<void*>(Loop)
        },
        {
            "nativePause",
            "()V",
            reinterpret_cast<void*>(Pause)
        },
        {
            "nativeRelease",
            "()V",
            reinterpret_cast<void*>(Release)
        },
        {
            "nativeTouchDown",
            "(FF)V",
            reinterpret_cast<void*>(TouchDown)
        },
        {
            "nativeTouchMove",
            "(FF)V",
            reinterpret_cast<void*>(TouchMove)
        },
        {
            "nativeTouchUp",
            "(FF)V",
            reinterpret_cast<void*>(TouchUp)
        }
    };
    if (env->RegisterNatives(env->FindClass("arsentyr/arkanoid/GameSurfaceView$NativeEngine"),
            methods, sizeof(methods) / sizeof(JNINativeMethod)) != JNI_OK) {
        return JNI_ERR;
    }
    return JNI_VERSION_1_6;
}
