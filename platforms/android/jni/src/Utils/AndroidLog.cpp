#include "Utils/AndroidLog.h"

#include <android/log.h>
#include "Assert.h"
#include "Config.h"

namespace Core {

void AndroidLog::Verbose(const char* format, ...) {
    ASSERT(format != NULL);
    if (Config::LOGGABLE_VERBOSE) {
        va_list args;
        va_start(args, format);
        __android_log_vprint(ANDROID_LOG_VERBOSE, LOG_TAG, format, args);
        va_end(args);
    }
}

void AndroidLog::Debug(const char* format, ...) {
    ASSERT(format != NULL);
    if (Config::LOGGABLE_DEBUG) {
        va_list args;
        va_start(args, format);
        __android_log_vprint(ANDROID_LOG_DEBUG, LOG_TAG, format, args);
        va_end(args);
    }
}

void AndroidLog::Warning(const char* format, ...) {
    ASSERT(format != NULL);
    if (Config::LOGGABLE_WARNING) {
        va_list args;
        va_start(args, format);
        __android_log_vprint(ANDROID_LOG_WARN, LOG_TAG, format, args);
        va_end(args);
    }
}

void AndroidLog::Error(const char* format, ...) {
    ASSERT(format != NULL);
    if (Config::LOGGABLE_ERROR) {
        va_list args;
        va_start(args, format);
        __android_log_vprint(ANDROID_LOG_ERROR, LOG_TAG, format, args);
        va_end(args);
    }
}

namespace {

AndroidLog androidLog;

} /* anonymous namespace */

ILog& Log = androidLog;

} /* namespace Core */
