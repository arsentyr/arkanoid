LOCAL_PATH := $(call my-dir)

MY_LOG_TAG := \"Arkanoid\"

include $(CLEAR_VARS)

LOCAL_MODULE := arkanoid

MY_SRC := ../../../src
LOCAL_SRC_FILES += \
    $(MY_SRC)/Application.cpp \
    $(MY_SRC)/Ball.cpp \
    $(MY_SRC)/Bonus.cpp \
    $(MY_SRC)/Chip.cpp \
    $(MY_SRC)/Core.cpp \
    $(MY_SRC)/Level.cpp \
    $(MY_SRC)/PositionColorShaderProgram.cpp \
    $(MY_SRC)/Racket.cpp

MY_SRC_GRAPHICS := $(MY_SRC)/Graphics
LOCAL_SRC_FILES += \
    $(MY_SRC_GRAPHICS)/BlendModeStack.cpp \
    $(MY_SRC_GRAPHICS)/ColorStack.cpp \
    $(MY_SRC_GRAPHICS)/Graphics.cpp \
    $(MY_SRC_GRAPHICS)/MatrixStack.cpp \
    $(MY_SRC_GRAPHICS)/ShaderProgram.cpp

MY_SRC_UTILS := $(MY_SRC)/Utils
LOCAL_SRC_FILES += \
    $(MY_SRC_UTILS)/Random.cpp \
    $(MY_SRC_UTILS)/Timer.cpp

LOCAL_SRC_FILES += \
    src/Utils/AndroidLog.cpp \
    src/arkanoid.cpp

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include

LOCAL_CFLAGS += -DLOG_TAG=$(MY_LOG_TAG)

LOCAL_LDLIBS += -llog -lGLESv2 -lz

include $(BUILD_SHARED_LIBRARY)
