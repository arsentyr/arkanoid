#ifndef CORE_ASSERT_H
#define CORE_ASSERT_H

#include <cstdlib>
#include <android/log.h>

#ifdef NDEBUG
#   define ASSERT(...) ((void)0)
#   define ASSERTF(...) ((void)0)
#else
#   define ASSERT(expr) ((expr) ? (void)0 : __android_log_assert(0, LOG_TAG, "Assertion failed: %s, file %s, line %d.", #expr, __FILE__, __LINE__))
#   define ASSERTF(expr, ...) do { if (!(expr)) { __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "ASSERT: " __VA_ARGS__); ASSERT(expr); } } while (false)
#endif /* NDEBUG */

#define VERIFY(expr, ...) do { if (!(expr)) { __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "VERIFY: " __VA_ARGS__); abort(); } } while (false)

#endif /* CORE_ASSERT_H */
