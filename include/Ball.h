#ifndef ARKANOID_BALL_H
#define ARKANOID_BALL_H

#include "Core.h"

namespace Arkanoid {

/* Vector */

class Vector {
public:
    float x;
    float y;

public:
    explicit Vector(float x = 0, float y = 0);

    Vector(const Vector& vec);

    Vector operator*(float scalar) const;

    Vector operator+(const Vector& vec) const;

    float Length() const;

    Vector& Normalize();
};

enum State {
    BALL_BOUNCE,
    BALL_BOUNCING,
    BALL_CATCH,
    BALL_CATCHING
};

/* Ball */

class Ball {
public:
    float x;
    float y;
    Vector velocity;
    State state;

public:
    Ball(float x, float y, float velocity);

    float Left() const;

    float Right() const;

    float Top() const;

    float Bottom() const;

    void SetVelocity(float xy);

    void InverseVelocityX();

    void InverseVelocityY();

    void Draw();

    static float Radius();
};

} /* namespace Arkanoid */

#endif /* ARKANOID_BALL_H */
