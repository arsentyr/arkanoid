#ifndef ARKANOID_POSITION_COLOR_SHADER_PROGRAM_H
#define ARKANOID_POSITION_COLOR_SHADER_PROGRAM_H

#include "Core.h"

namespace Arkanoid {

/* PositionColorShaderProgram */

class PositionColorShaderProgram : public ShaderProgram {
public:
    PositionColorShaderProgram();

    void Load();

    void Unload();

    // Graphics Callbacks

    virtual void Reload();

    virtual void MvpMatrixChanged();

    virtual void ColorChanged();

    virtual void SetVertexAttributes(const void* vertices);

private:
    PositionColorShaderProgram(const PositionColorShaderProgram&);

    PositionColorShaderProgram& operator=(const PositionColorShaderProgram&);

private:
    enum {
        UNKNOWN_UNIFORM_LOCATION = -1,
        ATTRIBUTE_POSITION_INDEX,
        ATTRIBUTE_COLOR_INDEX
    };

    GLint mUniformModelViewMatrixLocation;
    GLint mUniformProjectionMatrixLocation;
    GLint mUniformColorLocation;
};

} /* namespace Arkanoid */

#endif /* ARKANOID_POSITION_COLOR_SHADER_PROGRAM_H */
