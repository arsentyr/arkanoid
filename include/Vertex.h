#ifndef ARKANOID_VERTEX_H
#define ARKANOID_VERTEX_H

#include "Core.h"

namespace Arkanoid {

class Vertex {
public:
    float x;
    float y;
    Color color;

public:
	Vertex() : x(0), y(0), color(Color::White()) {}
	Vertex(float x, float y, const Color& color) : x(x), y(y), color(color) {}			
};

} /* namespace Arkanoid */

#endif /* ARKANOID_VERTEX_H */
