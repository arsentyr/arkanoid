#ifndef ARKANOID_LEVEL_H
#define ARKANOID_LEVEL_H

#include <list>
#include "Ball.h"
#include "Bonus.h"
#include "Chip.h"
#include "Levels.h"
#include "Racket.h"

namespace Arkanoid {

enum {
    BALL_MIN_VELOCITY = 300,
    BALL_MAX_VELOCITY = 550,
    CELL_WIDTH = 60,
    CELL_HEIGHT = 20,
    NUM_CATCH = 5,
    SLOW_TIME = 30
};

/* Level */

class Level {
public:
    Level();

    void Reset();

    void TouchDown(float x, float y);

    void TouchMove(float x, float y);

    void TouchUp(float x, float y);

    void Update(float deltaTime);

    void Draw();

private:
    void ResetRacket();

    void ResetBalls();

    void ResetChips();

    bool CheckChip(int row, int col);

private:
    typedef std::list<Ball> Balls;
    typedef std::list<Bonus> Bonuses;

    Racket mRacket;
    Balls mBalls;
    Chip* mChips[GRID_NUM_ROWS][GRID_NUM_COLS];
    int mNumChips;

    int mNumCatch;
    bool mHasSlow;
    float mSlowTime;
    Bonuses mBonuses;

    float mTouchX;
};

} /* namespace Arkanoid */

#endif /* ARKANOID_LEVEL_H */
