#ifndef ARKANOID_BONUS_H
#define ARKANOID_BONUS_H

namespace Arkanoid {

enum BonusType {
    BONUS_NONE,
    BONUS_CATCH,
    BONUS_DIVIDE,
    BONUS_SLOW
};

/* Bonus */

class Bonus {
public:
    Bonus(float x, float y, BonusType type);

    float Left() const;

    float Right() const;

    float Top() const;

    float Bottom() const;

    BonusType Type() const;

    void Update(float deltaTime);

    void Draw();

private:
    float mX;
    float mY;
    BonusType mType;
    static float sWidth;
    static float sHeight; 
};

} /* namespace Arkanoid */

#endif /* ARKANOID_BONUS_H */
