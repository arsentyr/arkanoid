#ifndef ARKANOID_RACKET_H
#define ARKANOID_RACKET_H

#include "Ball.h"
#include "Bonus.h"
#include "Core.h"
#include "Vertex.h"

namespace Arkanoid {

/* Racket */

class Racket {
public:
    void SetLocation(float x, float y, float width, float height);

    void SetOffsetX(float offsetX);

    float Left() const;

    float Right() const;

    float Top() const;

    float Bottom() const;

    bool Hit(const Ball& ball) const;

    bool Hit(const Bonus& bonus) const;

    void Draw();

private:
    Vertex mVertices[4];
};

} /* namespace Arkanoid */

#endif /* ARKANOID_RACKET_H */
