#ifndef CORE_RANDOM_H
#define CORE_RANDOM_H

namespace Core {

/* Random */

class Random {
public:
    Random();

    bool NextBool();

    int NextInt(int min, int max);

private:
    Random(const Random&);

    Random& operator=(const Random&);
};

} /* namespace Core */

#endif /* CORE_RANDOM_H */
