#ifndef CORE_ILOG_H
#define CORE_ILOG_H

namespace Core {

/* ILog */

class ILog {
public:
	ILog() {}

    virtual ~ILog() {}

    virtual void Verbose(const char* format, ...) = 0;

    virtual void Debug(const char* format, ...) = 0;

    virtual void Warning(const char* format, ...) = 0;

    virtual void Error(const char* format, ...) = 0;

private:
    ILog(const ILog&);

    ILog& operator=(const ILog&);
};

extern ILog& Log;

} /* namespace Core */

#endif /* CORE_ILOG_H */
