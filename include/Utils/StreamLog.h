#ifndef CORE_STREAM_LOG_H
#define CORE_STREAM_LOG_H

#include "Utils/ILog.h"

namespace Core {

/* StreamLog */

class StreamLog : public ILog {
public:
    // ILog Implementation

    virtual void Verbose(const char* format, ...);

    virtual void Debug(const char* format, ...);

    virtual void Warning(const char* format, ...);

    virtual void Error(const char* format, ...);
};

} /* namespace Core */

#endif /* CORE_STREAM_LOG_H */
