#ifndef CORE_TIMER_H
#define CORE_TIMER_H

#include <ctime>

namespace Core {

/* Timer */

class Timer {
public:
	Timer();

    void Start();

    void Stop();
    
    float GetDeltaTime();

private:
    Timer(const Timer&);

    Timer& operator=(const Timer&);

private:
    bool mIsStarted;
    clock_t mTime;
};

} /* namespace Core */

#endif /* CORE_TIMER_H */
