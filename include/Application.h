#ifndef ARKANOID_APPLICATION_H
#define ARKANOID_APPLICATION_H

#include <memory>
#include "Core.h"
#include "PositionColorShaderProgram.h"
#include "Level.h"

namespace Arkanoid {

/* Application */

class Application {
public:
    static Application& Instance();

    void Init();

    void Reload();

    void Resize(int widht, int height);

    void Resume();

    void Loop();

    void Pause();

    void Release();

    void TouchDown(float x, float y);

    void TouchMove(float x, float y);

    void TouchUp(float x, float y);

private:
    Application();

    Application(const Application&);

    Application& operator=(const Application&);

    void TransformTouch(float& x, float& y);

private:
    enum State {
        STATE_UNITIALIZED = 0,
        STATE_INITIALIZED,
        STATE_RESUMED,
        STATE_PAUSED,
        STATE_RELEASED
    }; 

    float mScale;
    float mHalfWidth;
    float mHalfHeight;
    State mState;
    Timer mTimer;
    PositionColorShaderProgram mShaderProgram;
    Level mLevel;
};

} /* namespace Arkanoid */

#endif /* ARKANOID_APPLICATION_H */
