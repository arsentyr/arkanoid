#ifndef CORE_SHADER_PROGRAM_H
#define CORE_SHADER_PROGRAM_H

#include "Graphics/GLES2.h"

namespace Core {

/* ShaderProgram */

class ShaderProgram {
public:
    ShaderProgram();

    virtual ~ShaderProgram();

    void Use();

    // Graphics Callbacks

    virtual void Reload() = 0;
    
    virtual void MvpMatrixChanged() = 0;

    virtual void ColorChanged() = 0;

    virtual void SetVertexAttributes(const void* vertices) = 0;

protected:
    // Life cycle
    
    void Create(const GLchar* sourceVertexShader, const GLchar* sourceFragmentShader);

    void Link();

    void Delete();

    void Lose();

    // Attributes

    void BindAttributeLocation(GLuint index, const GLchar* name);

    static void SetVertexAttributePointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* pointer);

    static void EnableVertexAttributeArray(GLuint index);

    // Uniforms

    GLint GetUniformLocation(const GLchar* name) const;

    static void SetUniform4fv(GLint location, GLsizei count, const GLfloat* fvalues);

    static void SetUniformMatrix4fv(GLint location, GLsizei count, const GLfloat* fvalues);

private:
    ShaderProgram(const ShaderProgram&);

    ShaderProgram& operator=(const ShaderProgram&);

    bool IsProgram() const;

private:
    static const GLuint UNKNOWN_PROGRAM = 0;
    static const GLuint UNKNOWN_SHADER = 0;

    GLuint mProgram;
    GLuint mVertexShader;
    GLuint mFragmentShader;
};

} /* namespace Core */

#endif /* CORE_SHADER_PROGRAM_H */
