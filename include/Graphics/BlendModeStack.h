#ifndef CORE_BLEND_MODE_STACK_H
#define CORE_BLEND_MODE_STACK_H

#include <vector>

namespace Core {

enum BlendMode {
    BLEND_NORMAL,
    BLEND_ALPHA,
    BLEND_ADD,
    BLEND_MULTIPLY
};

/* BlendModeStack */

class BlendModeStack {
public:
    explicit BlendModeStack(unsigned int capacity);

    BlendModeStack& operator=(BlendMode blendMode);

    operator BlendMode () const;

    void Push(BlendMode blendMode);

    void Pop();

    BlendMode Top() const;

    unsigned int Depth() const;

private:
    BlendModeStack(const BlendModeStack&);

    BlendModeStack& operator=(const BlendModeStack&);

    void SetBlendMode(BlendMode blendMode);

private:
    std::vector<BlendMode> mStack;
};

} /* namespace Core */

#endif /* CORE_BLEND_MODE_STACK_H */
