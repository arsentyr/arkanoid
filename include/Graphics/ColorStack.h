#ifndef CORE_COLOR_STACK_H
#define CORE_COLOR_STACK_H

#include <vector>

namespace Core {

/* Color */

#define CHECK_COLOR(color) ASSERT(color.red >= 0 && color.red <= 1 && color.green >= 0 && color.green <= 1 && color.blue >= 0 && color.blue <= 1 && color.alpha >= 0 && color.alpha <= 1)

class Color {
public:
    static Color White() {
        return Color(1, 1, 1, 1);
    }

    static Color Black() {
        return Color(0, 0, 0, 0);
    }

public:
    float red;
    float green;
    float blue;
    float alpha;

public:
    Color()
            : red(0)
            , green(0)
            , blue(0)
            , alpha(0) {
    }

    Color(float red, float green, float blue, float alpha = 1)
            : red(red)
            , green(green)
            , blue(blue)
            , alpha(alpha) {
    }

    operator const float* () const {
        return &red;
    }
};

/* ColorStack */

class ColorStack {
public:
    explicit ColorStack(unsigned int capacity);

    ColorStack& operator=(const Color& color);

    operator const Color& () const;

    void Push(float red, float green, float blue, float alpha = 1);

    void Push(const Color& color);

    void Pop();

    const Color& Top() const;

    unsigned int Depth() const;

protected:
    // for friend class Graphics
    
    bool HasChanged() const;

    void Unchanged();

private:
    ColorStack(const ColorStack&);

    ColorStack& operator=(const ColorStack&);

private:
    bool mHasChanged;
    std::vector<Color> mStack;

friend class Graphics;
};

} /* namespace Core */

#endif /* CORE_COLOR_STACK_H */
