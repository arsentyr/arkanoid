#ifndef CORE_GRAPHICS_H
#define CORE_GRAPHICS_H

#include "Graphics/BlendModeStack.h"
#include "Graphics/ColorStack.h"
#include "Graphics/GLES2.h"
#include "Graphics/MatrixStack.h"
#include "Graphics/ShaderProgram.h"

namespace Core {

/* Graphics */

class Graphics {
public:
    Graphics();

    ~Graphics();

    void Init();

    void Reload();

    void Release();

    void SetViewport(int x, int y, int width, int height);

    void SetOrtho(float xLeft, float xRight, float yBottom, float yTop, float zNear, float zFar);

    void Begin();

    void End();

    void SetLineWidth(float lineWidth);

    MatrixStack& ModelView() { return mModelViewMatrixStack; }

    MatrixStack& Projection() { return mProjectionMatrixStack; }

    ColorStack& Color() { return mColorStack; }

    BlendModeStack& BlendMode() { return mBlendModeStack; }

    void Bind(ShaderProgram* shaderProgram);

    void DrawLines(const void* vertices, int count);

    void DrawLineLoop(const void* vertices, int count);

    void DrawLineStrip(const void* vertices, int count);

    void DrawTriangleStrip(const void* vertices, int count);
    void DrawTriangleStrip(const void* vertices, const unsigned short* indices, int count);

    void DrawTriangleFan(const void* vertices, int count);

private:
    Graphics(const Graphics&);

    Graphics& operator=(const Graphics&);

    void CheckChanges();

    void DrawArrays(GLenum mode, const GLvoid* vertices, GLsizei count);

    void DrawElements(GLenum mode, const GLvoid* vertices, const GLvoid* indices, GLsizei count);

private:
    enum State {
        STATE_UNITIALIZED,
        STATE_INITIALIZED,
        STATE_FRAMED,
        STATE_RELEASED
    };

    State mState;
    MatrixStack mModelViewMatrixStack;
    MatrixStack mProjectionMatrixStack;
    Matrix4 mProjectionMatrix;
    ColorStack mColorStack;
    BlendModeStack mBlendModeStack;
    ShaderProgram* mShaderProgram;
};

extern Graphics& Gfx; 

} /* namespace Core */

#endif /* CORE_GRAPHICS_H */
