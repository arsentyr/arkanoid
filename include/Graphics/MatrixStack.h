#ifndef CORE_MATRIX_STACK_H
#define CORE_MATRIX_STACK_H

#include <vector>

namespace Core {

/* Matrix4 */

class Matrix4 {
public:
    static Matrix4 Identity() {
        return Matrix4(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
        );
    }

public:
    float x0, x1, x2, x3;
    float y0, y1, y2, y3;
    float z0, z1, z2, z3;
    float w0, w1, w2, w3;

public:
    Matrix4(float x0, float x1, float x2, float x3,
            float y0, float y1, float y2, float y3,
            float z0, float z1, float z2, float z3,
            float w0, float w1, float w2, float w3)
            : x0(x0), x1(x1), x2(x2), x3(x3)
            , y0(y0), y1(y1), y2(y2), y3(y3)
            , z0(z0), z1(z1), z2(z2), z3(z3)
            , w0(w0), w1(w1), w2(w2), w3(w3) {
    }

	Matrix4(const Matrix4& mat)
			: x0(mat.x0), x1(mat.x1), x2(mat.x2), x3(mat.x3)
            , y0(mat.y0), y1(mat.y1), y2(mat.y2), y3(mat.y3)
            , z0(mat.z0), z1(mat.z1), z2(mat.z2), z3(mat.z3)
            , w0(mat.w0), w1(mat.w1), w2(mat.w2), w3(mat.w3) {
    }	 

    operator const float* () const {
        return &x0;
    }
};

/* MatrixStack */

class MatrixStack {
public:
    explicit MatrixStack(unsigned int capacity);

    MatrixStack& operator=(const Matrix4& matrix);

    operator const Matrix4& () const;

    void Push();

    void Pop();

    const Matrix4& Top() const;

    unsigned int Depth() const;

    void Scale(float scaleX, float scaleY, float scaleZ = 1);

    void Translate(float deltaX, float deltaY, float deltaZ = 0);

    void RotateZ(float degrees);

protected:
    // for friend class Graphics
    
    bool HasChanged() const;

    void Unchanged();

private:
    MatrixStack(const MatrixStack&);

    MatrixStack& operator=(const MatrixStack&);

private:
    bool mHasChanged;
    std::vector<Matrix4> mStack;

friend class Graphics;
};

} /* namespace Core */

#endif /* CORE_MATRIX_STACK_H */
