#ifndef ARKANOID_CHIP_H
#define ARKANOID_CHIP_H

#include "Bonus.h"
#include "Vertex.h"

namespace Arkanoid {

/* Chip */

class Chip {
public:
    Chip(float x, float y, float width, float height, Color color, BonusType bonus);

    float Left() const;

    float Right() const;

    float Top() const;

    float Bottom() const;

    BonusType Bonus() const;

    void Draw();

private:
    Vertex mVertices[4];
    BonusType mBonus;
};

} /* namespace Arkanoid */

#endif /* ARKANOID_CHIP_H */
