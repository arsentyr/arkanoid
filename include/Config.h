#ifndef CORE_CONFIG_H
#define CORE_CONFIG_H

namespace Core {

/* Config */

class Config {
public:
#ifdef NDEBUG
    static const bool DEBUG = false;
    static const bool LOGGABLE_VERBOSE = false;
    static const bool LOGGABLE_DEBUG = false;
    static const bool LOGGABLE_WARNING = true;
    static const bool LOGGABLE_ERROR = true;
#else
    static const bool DEBUG = true;
    static const bool LOGGABLE_VERBOSE = true;
    static const bool LOGGABLE_DEBUG = true;
    static const bool LOGGABLE_WARNING = true;
    static const bool LOGGABLE_ERROR = true;
#endif /* NDEBUG */

private:
    Config(const Config&);

    Config& operator=(const Config&);
};

} /* namespace Core */

#endif /* CORE_CONFIG_H */
