#ifndef CORE_H
#define CORE_H

#include "Assert.h"
#include "Config.h"
#include "Graphics/Graphics.h"
#include "Math/Math.h"
#include "Utils/ILog.h"
#include "Utils/Timer.h"
#include "Utils/Random.h"

namespace Arkanoid {

// Config
using Core::Config;
// Graphics
using Core::BLEND_ALPHA;
using Core::BLEND_ADD;
using Core::Color;
using Core::Gfx;
using Core::ShaderProgram;
// Math
using Core::Min;
using Core::Max;
using Core::Cos;
using Core::Sin;
using Core::Sqrt;
using Core::RadToDeg;
using Core::DegToRad;
// Utils
using Core::Log;
using Core::Timer;
extern Core::Random& Rand;

static const float LOGICAL_WIDTH = 480;
static const float LOGICAL_HEIGHT = 800;

} /* namespace Arkanoid  */

#endif /* CORE_H */
